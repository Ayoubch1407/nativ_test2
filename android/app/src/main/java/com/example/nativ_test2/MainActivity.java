package com.example.nativ_test2;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;

public class MainActivity extends FlutterActivity {
    boolean test = false;
    BroadcastReceiver receiver ;
    String myLocation ;



    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);

        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), "BackgroundService")
                .setMethodCallHandler((call, result) -> {
                    //Intent forService = new Intent(this, TestService.class);
                    Intent forService = new Intent(this, LocationService.class);
                    if(call.method.equals("startService")){
                        //callService(forService);
                        runtime_permissions();
                        startService(forService);
                        result.success("Service Started");
                    }else if(call.method.equals("stopService")){
                        stopService(forService);
                        result.success("Service Stopped");
                    }
                });
    }


    private void callService(Intent serv){
        runtime_permissions();
        if(test){

            startService(serv);
        }else{
            Toast.makeText(this, "Donne les permission !!", Toast.LENGTH_SHORT).show();
            callService(serv);
        }


    }
    private boolean runtime_permissions(){
        if(Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){

            String tabPermissions[]={Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            requestPermissions(tabPermissions, 100);
            return true;
        }

        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 100){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                test = true;
            }else{
                runtime_permissions();
            }
        }
    }

    /*
    @Override
    protected void onResume() {
        super.onResume();
        if(receiver == null){
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    myLocation = "Location: "+intent.getExtras().get("location");
                }
            };
        }
        registerReceiver(receiver, new IntentFilter("location_update"));
    }
     */

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(receiver != null){
            unregisterReceiver(receiver);
        }

    }
}
