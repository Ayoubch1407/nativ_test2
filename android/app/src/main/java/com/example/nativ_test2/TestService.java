package com.example.nativ_test2;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import androidx.core.app.ActivityCompat;
import io.flutter.embedding.android.FlutterView;
import io.flutter.embedding.engine.FlutterEngine;

public class TestService extends Service {
    private static final int REQUEST_LOCATION = 1;
    public static final int delay = 2000;
    private static final int dist = 1000;
    private Handler handler = new Handler();
    private Timer timer = null;
    Location myLocation = null;
    LocationManager locMgr = null;
    int count = 0;




    private LocationListener GPSListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            // update location
            myLocation = location;
            //locMgr.removeUpdates(GPSListener); // remove this listener
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };


    public TestService() {
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (timer != null) // Cancel if already existed
            timer.cancel();
        else
            timer = new Timer();   //recreate new

        timer.scheduleAtFixedRate(new TimeDisplay(), 0, delay);   //Schedule task   //Schedule task

        //Toast.makeText(getApplicationContext(), "This is from Android Service", Toast.LENGTH_SHORT).show();
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timer != null) timer.cancel(); //For Cancel Timer
        Toast.makeText(this, "Service is Destroyed", Toast.LENGTH_SHORT).show();
    }



    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }





    private Location obtainLocation() {
        Location gpslocation = null;
        if (locMgr == null)
            locMgr = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (isLocationEnabled()) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                System.out.println("Problem dans les droits d'acces");
                String tabPermissions[]={Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
                //ActivityCompat.requestPermissions(getApplicationContext().,tabPermissions, 101);
            }
            locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, delay, dist,  GPSListener);
            locMgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, delay, dist, GPSListener);
            gpslocation = locMgr.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (isLocationEnabled()){
                locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        delay, dist, GPSListener);
            }

        }else{
            System.out.println("Problem dans le provider");;
        }
        return gpslocation;
    }







    //class TimeDisplay for handling task
    class TimeDisplay extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            count++;
            handler.post(() -> {
                locMgr = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                myLocation = obtainLocation();
                if(myLocation != null){
                    String latitude = "lat : " + myLocation.getLatitude();
                    String longitude = "long : " + myLocation.getLongitude();
                    Toast.makeText(TestService.this, "Location : "+latitude+", "+longitude,
                            Toast.LENGTH_SHORT).show();
                }else
                    Toast.makeText(TestService.this, "Erreur ---- "+count, Toast.LENGTH_SHORT).show();
                // display toast
                //Toast.makeText(TestService.this, "Service is running"+count, Toast.LENGTH_SHORT).show();
            });
        }
    }
}