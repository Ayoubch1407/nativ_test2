import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  MethodChannel platform = new MethodChannel('BackgroundService');



  void startService() async{
    dynamic value = await platform.invokeMethod('startService');
    print(value);
  }

  void stopService() async{
    dynamic value = await platform.invokeMethod('stopService');
    print(value);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              onPressed: startService,
              child: Text('Start Background Service'),
            ),
            SizedBox(height: 6.0,),
            ElevatedButton(
              onPressed: stopService,
              child: Text('Stop Background Service'),
            ),
          ],
        ),
      ),
    );
  }
}
