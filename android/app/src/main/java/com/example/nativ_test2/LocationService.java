package com.example.nativ_test2;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;

public class LocationService extends Service {

    private LocationListener listener;
    private LocationManager locationManager;
    private Location location ;
    private Handler handler = new Handler();
    private Timer timer = null;
    public static final int delay = 2000;
    int count = 0;

    @SuppressLint("MissingPermission")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        listener = new LocationListener() {
            @Override
            public void onLocationChanged(@NonNull Location location) {
                /*Intent i = new Intent("location_update");
                i.putExtra("location", location.getLatitude()+","+location.getLongitude());
                sendBroadcast(i);*/
            }

            @Override
            public void onProviderDisabled(@NonNull String provider) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }

            @Override
            public void onProviderEnabled(@NonNull String provider) {

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }
        };
        /*locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 0, listener);
        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Intent i = new Intent("location_update");
        if(location != null)
            i.putExtra("location", location.getLatitude()+","+location.getLongitude());
        else
            i.putExtra("location", "null");
        sendBroadcast(i);*/

        if (timer != null) // Cancel if already existed
            timer.cancel();
        else
            timer = new Timer();   //recreate new

        timer.scheduleAtFixedRate(new TimeDisplay(), 0, delay);   //Schedule task   //Schedule task

        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timer != null) timer.cancel(); //For Cancel Timer
        Toast.makeText(this, "Service is Destroyed", Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("MissingPermission")
    private Location getLastKnownLocation() {
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        Location oldLoc;
        while (true){
            oldLoc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (oldLoc == null){
                continue;
            }
            else {
                break;
            }
        }
        return oldLoc;
    }


    //class TimeDisplay for handling task
    class TimeDisplay extends TimerTask {
        @SuppressLint("MissingPermission")
        @Override
        public void run() {
            // run on another thread
            count++;
            handler.post(() -> {
                locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, listener);
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                //location = getLastKnownLocation();
                if(location != null)
                    Toast.makeText(LocationService.this, "Location ("+count+") : "+location.getLatitude()+","+location.getLongitude(), Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(LocationService.this, "Location ("+count+"): Null", Toast.LENGTH_SHORT).show();

                // display toast
                //Toast.makeText(TestService.this, "Service is running"+count, Toast.LENGTH_SHORT).show();
            });
        }
    }
}